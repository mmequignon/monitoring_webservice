#!/usr/bin/env python3

import os

from flask_restful import Api
from flask import Flask

from influxdb_api.endpoints.raid import Raid
from influxdb_api.endpoints.volume import Volume

endpoints = [
    Raid,
    Volume,
]

env = os.environ


def main():
    app = Flask(__name__)
    api = Api(app)
    for endpoint in endpoints:
        api.add_resource(endpoint, endpoint.route)
    app.run(host="0.0.0.0", port=env.get("PORT", 5000))


if __name__ == "__main__":
    main()
