#!/usr/bin/env python3

import unittest


class TestCommon(unittest.TestCase):
    def check_schema(self, validator, expected):
        self.assertTrue(validator.validate(expected))
