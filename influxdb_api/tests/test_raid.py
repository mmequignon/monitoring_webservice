from .test_common import TestCommon
from ..endpoints.raid import Raid

DATA = {
    "POST": {
        "input": {
            "fields": {
                "state": True,
            },
            "tags": {
                "disk_name": "/dev/sda",
                "partition_name": "/dev/sda1",
            },
        }
    }
}


class TestRaid(TestCommon):
    def setUp(self):
        self.endpoint = Raid()

    def test_post_input_schema(self):
        for method, data in DATA.items():
            data_input = data["input"]
            endpoint_schemas = self.endpoint.methods[method]
            input_schema = endpoint_schemas["input"]
            self.check_schema(input_schema, data_input)
