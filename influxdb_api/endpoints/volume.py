#!/usr/bin/env python3

from flask import request
from flask_restful import Resource

from .common import Endpoint
from influxdb_api.utils.tools import http_wrapper
from influxdb_api.utils.validators import percentage


class Volume(Resource, Endpoint):

    route = "/volume"
    influxdb_table_name = "volume"

    @property
    def fields_schema(self):
        return {
            "usage": {
                "type": "float",
                "required": True,
                "check_with": percentage,
            },
        }

    @property
    def tags_schema(self):
        return {
            "partition_name": {"type": "string", "required": True},
        }

    @property
    def post_input_schema(self):
        res = self.common_input_schema
        return res

    def _get_methods(self):
        return ["POST"]

    @http_wrapper
    def post(self):
        return request.json
