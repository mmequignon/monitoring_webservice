#!/usr/bin/env python3

from flask import request
from flask_restful import Resource

from .common import Endpoint
from influxdb_api.utils.tools import http_wrapper


class Raid(Resource, Endpoint):

    route = "/raid"
    influxdb_table_name = "raid"

    @property
    def fields_schema(self):
        return {
            "state": {"type": "boolean", "required": True},
        }

    @property
    def tags_schema(self):
        return {
            "disk_name": {"type": "string", "required": True},
            "partition_name": {"type": "string", "required": True},
        }

    @property
    def post_input_schema(self):
        res = self.common_input_schema
        return res

    def _get_methods(self):
        return ["POST"]

    @http_wrapper
    def post(self):
        return request.json
