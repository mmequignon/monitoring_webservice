#!/usr/bin/env python3

from cerberus import Validator


class Endpoint:
    def __init__(self):
        if not self.route:
            raise NotImplementedError(f"route isn't set on {self.__name__}")
        if not self.influxdb_table_name:
            raise NotImplementedError(
                f"influxdb_table_name isn't set on {self.__name__}"
            )
        self.setup_methods()

    @property
    def fields_schema(self):
        raise NotImplementedError

    @property
    def tags_schema(self):
        raise NotImplementedError

    @property
    def common_input_schema(self):
        return {
            "fields": {
                "type": "dict",
                "required": True,
                "schema": self.fields_schema,
            },
            "tags": {
                "type": "dict",
                "required": True,
                "schema": self.tags_schema,
            },
        }

    def setup_methods(self):
        self.methods = {}
        for method in self._get_methods():
            input_schema_attr = f"{method.lower()}_input_schema"
            input_schema = getattr(self, input_schema_attr, None)
            if not (input_schema):
                raise NotImplementedError
            output_schema_attr = f"{method.lower()}_output_schema"
            output_schema = getattr(self, output_schema_attr, None)
            self.methods[method] = {
                "input": Validator(input_schema),
                "output": Validator(output_schema) if output_schema else None,
            }

    def _get_methods(self):
        raise NotImplementedError
