#!/usr/bin/env python3


def percentage(field, value, error):
    if not 0 <= value <= 100:
        error(field, "should be a percentage")
