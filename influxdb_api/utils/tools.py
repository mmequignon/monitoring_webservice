#!/usr/bin/env python3

import pytz

from datetime import datetime
from werkzeug.exceptions import BadRequest

from functools import wraps
from influxdb import InfluxDBClient
from flask import request

_client = None

DATABASE = "monitoring"
DT_FMT = "%Y-%m-%dT%H:%M:%S.%f"


class DbClient:

    _client = None
    database_name = "monitoring"

    @property
    def client(self):
        if self._client is None:
            self._client = InfluxDBClient()
            self.ensure_database()
        self._client.switch_database(self.database_name)
        return self._client

    def ensure_database(self):
        databases = self._client.get_list_database()
        exists = any(
            self.database_name == database["name"] for database in databases
        )
        if not exists:
            self._client.create_database(self.database_name)

    def write_points(self, vals):
        return self.client.write_points(vals)

    def query(self, request):
        return self.client.query(request)


def http_wrapper(func):
    def validate_input(endpoint):
        method = endpoint.methods[request.method]
        input_validator = method["input"]
        if not input_validator.validate(request.json):
            raise BadRequest(input_validator.errors)
        return True

    def validate_output(endpoint, output):
        method = endpoint.methods[request.method]
        output_validator = method.get("output")
        if output_validator and not output_validator.validate(output):
            raise BadRequest(output_validator.errors)
        return True

    def push_to_influx_db(measurement, vals):
        now = pytz.timezone("Europe/Paris").localize(datetime.now(), DT_FMT)
        res = [
            {
                "measurement": measurement,
                "fields": vals["fields"],
                "tags": vals["tags"],
                "time": now,
            }
        ]
        client = DbClient()
        res = client.write_points(res)
        return res

    @wraps(func)
    def wrapper(endpoint):
        validate_input(endpoint)
        res = func(endpoint)
        validate_output(endpoint, res)
        if res:
            measurement = endpoint.influxdb_table_name
            push_to_influx_db(measurement, res)
        return res

    return wrapper
