with import <nixpkgs> {};
with python39Packages; 

let 
  pythonEnv = withPackages [
    flask
  ];
in buildPythonPackage {

  name = "influxdb_api";
  src = ./.;

  propagatedBuildInputs = [
    flask
    influxdb
    requests
    cerberus
    flask-restful
    pytz
  ];

  checkInputs = [
    black
    flake8
  ];

  checkPhase = ''
    echo -e "\x1b[32m## run unittest\x1b[0m"
    python -m unittest
    echo -e "\x1b[32m## run flake8\x1b[0m"
    black --check --config=config/black.toml .
    echo -e "\x1b[32m## run flake8\x1b[0m"
    flake8 --config=config/flake8.conf .
  '';
}
