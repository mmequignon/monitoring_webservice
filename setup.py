#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name="InfluxDB API",
    packages=find_packages("influxdb_api", exclude=["tests"]),
    install_requires=[
        "flask",
        "influxdb",
        "requests",
        "cerberus",
        "flask-restful",
        "pytz",
    ],
    entry_points={
        "console_scripts": ["influxdb-api=influxdb_api.__main__:main"],
    },
    test_suite="influxdb_api/tests",
)
